package mk.ukim.finki.networking.filetransfer;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client extends Thread{

    String serverAddress;
    int port;
    String downloadFolder;


    public Client(String serverAddress, int port, String downloadFolder) {
        this.serverAddress = serverAddress;
        this.port = port;
        this.downloadFolder = downloadFolder;
    }

    @Override
    public void run() {
        Socket socket = null;
        DataInputStream dis = null;
        DataOutputStream dos = null;
        try {
            socket = new Socket(this.serverAddress, port);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            List<String> files = listFiles(dis,dos);
            showFilesToUser(files);
            String filename = chooseFileToDownload(files);
            initFileDownload(dis,dos,filename);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> listFiles(DataInputStream dis, DataOutputStream dos) throws IOException {
        dos.writeUTF(Constants.LIST_FILES_CMD);
        String response = dis.readUTF();
        List<String> files = new ArrayList<>();
        while (!(response=dis.readUTF()).equals(Constants.LIST_FILES_FINISH_CMD)) {
            files.add(response);
        }
        return files;
    }

    private void showFilesToUser(List<String> files) {
        for (int i=0;i<files.size();i++) {
            System.out.printf("%d %s\n",i+1,files.get(i));
        }
    }

    private String chooseFileToDownload(List<String> files) {
        System.out.println("Client:Please choose the id of file to download.");
        Scanner scanner = new Scanner(System.in);
        int fileNumber = scanner.nextInt();
        return files.get(fileNumber-1);
    }

    private void initFileDownload(DataInputStream dis, DataOutputStream dos, String filename) throws IOException {
        dos.writeUTF(Constants.DOWNLOAD_FILE_NAME +filename);
        String response = dis.readUTF();
        if (response.startsWith(Constants.DOWNLOAD_FILE_START)) {
            System.out.println("Client:File Download is starting...");
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(String.format("%s%s",downloadFolder,filename)));
                while (!(response=dis.readUTF()).equals(Constants.DOWNLOAD_FILE_FINISH)) {
                    writer.write(response+"\n");

                }
            } finally {
                if (writer!=null) {
                    writer.flush();
                    writer.close();
                }
            }
            System.out.println("Client:File Download finished!");
        }
    }
}
