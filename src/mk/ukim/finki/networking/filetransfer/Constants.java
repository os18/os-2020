package mk.ukim.finki.networking.filetransfer;

public class Constants {

    public final static String SERVER_IP = "localhost";
    public final static int SERVER_PORT = 4500;
    public final static String PUBLIC_FOLDER = "C:\\nastava\\OS2022\\data\\";
    public final static String DOWNLOAD_FOLDER = "C:\\nastava\\OS2022\\data\\downloads\\";

    public final static String LIST_FILES_CMD = "listFiles";

    public final static String LIST_FILES_START_CMD = "listFiles:start";
    public final static String LIST_FILES_FINISH_CMD = "listFiles:finish";

    public final static String DOWNLOAD_FILE_NAME ="downloadfile?name=";
    public final static String DOWNLOAD_FILE_START = "downloadfile:start";
    public final static String DOWNLOAD_FILE_FINISH = "downloadfile:finished";

}
