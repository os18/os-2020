package mk.ukim.finki.networking.filetransfer;

public class Main {

    public static void main(String[] args) {
        Server server = new Server(Constants.SERVER_PORT, Constants.PUBLIC_FOLDER);
        server.start();

        Client  client = new Client(Constants.SERVER_IP, Constants.SERVER_PORT, Constants.DOWNLOAD_FOLDER);
        client.start();
    }
}
