package mk.ukim.finki.networking.filetransfer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread{

    int port;
    private ServerSocket serverSocket;
    private String publicFolder;

    public Server(int port, String publicFolder) {
        this.port = port;
        this.publicFolder = publicFolder;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(this.port);
            while(true) {
                Socket socket = this.serverSocket.accept();
                SocketWorker worker = new SocketWorker(socket, this.publicFolder);
                worker.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
