package mk.ukim.finki.networking.filetransfer;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

public class SocketWorker extends Thread{

    private Socket socket;
    private String publicFolder;

    public SocketWorker(Socket socket, String publicFolder) {
        this.socket = socket;
        this.publicFolder = publicFolder;
    }

    @Override
    public void run() {
        DataInputStream dis = null;
        DataOutputStream dos = null;
        try {
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            String command = dis.readUTF();
            if (Constants.LIST_FILES_CMD.equals(command)) {

                System.out.println("Server:List Files Command Received!");
                List<String> files = listFiles();
                shareListedFiles(dos,files);
            }
            String command2 = dis.readUTF();
            if (command2.startsWith(Constants.DOWNLOAD_FILE_NAME)) {
                System.out.println("Server:Download File Command Received!");
                String filename = command2.split("=")[1];
                System.out.println("Server:File Uploading is starting...");
                uploadFileToClient(dos,filename);
                System.out.println("Server:File Uploading finished!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> listFiles() {
        File publicFolderFile = new File(this.publicFolder);
        return Arrays.stream(publicFolderFile.listFiles()).filter(v->{
            return v.isFile();
        }).map(v->{
            return v.getName();
        }).toList();
    }

    private void shareListedFiles(DataOutputStream dos, List<String> files) throws IOException {
        dos.writeUTF(Constants.LIST_FILES_START_CMD);
        for (String f:files) {
            dos.writeUTF(f);
        }
        dos.writeUTF(Constants.LIST_FILES_FINISH_CMD);
    }

    private void uploadFileToClient(DataOutputStream dos, String filename) throws IOException {
        dos.writeUTF(Constants.DOWNLOAD_FILE_START);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(String.format("%s%s",publicFolder,filename)));
            String line = null;
            while ((line=reader.readLine())!=null) {
                dos.writeUTF(line);
            }
            dos.writeUTF(Constants.DOWNLOAD_FILE_FINISH);
        } finally {
            reader.close();
        }
    }
}
