package mk.ukim.finki.threading;

public class Application {

    public static void main(String[] args) {
        SharedResource sr1 = new SharedResource();
        SharedResource sr2 = new SharedResource();
        SharedResource sr3 = new SharedResource();

        SequenceIncrementThread t1 = new SequenceIncrementThread(sr1);

        SequenceIncrementThread t2 = new SequenceIncrementThread(sr2);

        SequenceIncrementThread t3 = new SequenceIncrementThread(sr3);

        t1.start();
        t2.start();
        t3.start();
    }
}
