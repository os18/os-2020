package mk.ukim.finki.threading.matrix;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        System.out.println("Vnesete ja prvata matrica:");
        Matrix m1 = Matrix.buildFromStdIn();
        m1.print();

        System.out.println("Vnesete ja vtorata matrica:");
        Matrix m2 = Matrix.buildFromStdIn();
        m2.print();

        if (m1.getN() != m2.getM()) {
            throw new RuntimeException("not appropriate");
        }

        Matrix result = new Matrix(m1.getM(),m2.getN());

        List<Semaphore> semaphoreList = new ArrayList<>();
        List<MatrixMultiplier> mmList = new ArrayList<>();

        for (int i=0;i<m1.getM();i++) {
            for (int j=0;j<m2.getN();j++) {
                Semaphore s = new Semaphore(0);
                semaphoreList.add(s);
                mmList.add(new MatrixMultiplier(i,j,m1.getRow(i),m2.getColumn(j),result,s));

            }
        }

        for (MatrixMultiplier m:mmList) {
            m.start();
        }

        //mainThread-ot mora da gi pocheka site MatrixMultiplier thread-ovi da zavrshat
//        for (MatrixMultiplier m:mmList) {
//            m.join();
//        }

        for (Semaphore s:semaphoreList) {
            s.acquire();
        }
        System.out.println("Rezultatot e:");
        result.print();

    }
}
