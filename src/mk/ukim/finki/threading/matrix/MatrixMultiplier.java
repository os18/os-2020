package mk.ukim.finki.threading.matrix;

import java.util.concurrent.Semaphore;

public class MatrixMultiplier extends Thread{

    int i,j;
    double[] row;
    double[] column;
    Matrix result;
    Semaphore semaphore;

    public MatrixMultiplier(int i, int j, double[] row, double[] column, Matrix result, Semaphore semaphore) {
        this.i = i;
        this.j = j;
        this.row = row;
        this.column = column;
        this.result = result;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        if (row.length != column.length) {
            throw new RuntimeException("not appropriate");
        }
        double value = 0.0;
        for (int ii=0;ii<row.length;ii++) {
            value += row[ii]*column[ii];
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result.setEl(i,j,value);
        semaphore.release();
    }
}
